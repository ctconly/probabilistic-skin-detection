// DetectSkin.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


int main(int argc, char** argv)
{
  // Make sure they're using it correctly
  if (argc > 4)
  {
    printf("Usage:\n To use a file:\n DetectSkin <image>\nor\nDetectSkin <image> <P(S)> <threshold>\n\n");
    printf("To use webcam:\n DetectSkin\nor\n DetectSkin <P(S)> <threshold>\n\n");
    return -1;
  }

  // Set our initial skin/!skin probabilities and threshold for showing a pixel as potential skin in the image
  double ps = 0.25;
  double threshold = 0.3;
  if (argc == 4)
  {
    ps = atof(argv[2]);
    threshold = atof(argv[3]);
  }
  else if (argc == 3)
  {
    ps = atof(argv[1]);
    threshold = atof(argv[2]);
  }
  double pns = 1.0 - ps;
  printf("%.1f, %.1f, %.1f\n", ps, pns, threshold);

  // Create our detector and make sure it was success
  ProbSkinDetector psd("positives.bin", "negatives.bin", ps, threshold);
  if (!psd.success) 
    return -1;

  cv::Mat img;            // Either the image from the file or a webcam frame
  cv::Mat backprojection; // The probability image after running skin detection on img

  if (argc == 2 || argc == 4)
  {
    // Try to read the image; exit if we can't
    char *imageFile = argv[1];
    img = cv::imread(imageFile, cv::IMREAD_COLOR);
    if (!img.data)
    {
      printf("No image data.\n");
      return -1;
    }

    // We successfully read the image, so detect the skin in img
    backprojection = psd.detectSkin(img);

    // Display the normal image
    cv::namedWindow(imageFile, cv::WINDOW_AUTOSIZE);
    cv::imshow(imageFile, img);

    // Display the backprojection image
    cv::namedWindow("backprojection", cv::WINDOW_AUTOSIZE);
    cv::imshow("backprojection", backprojection);
    cv::waitKey(0);

    return 0;
  }

  cv::VideoCapture cap(0);
  if (!cap.isOpened())
  {
    printf("Error opening webcam.\n");
    return -1;
  }

  cv::namedWindow("Webcam", CV_WINDOW_AUTOSIZE);

  for (;;)
  {
    cap >> img;
    backprojection = psd.detectSkin(img);

    // Display the normal image
    cv::imshow("Webcam", img);

    // Display the backprojection image
    cv::namedWindow("backprojection", cv::WINDOW_AUTOSIZE);
    cv::imshow("backprojection", backprojection);

    // Quit if user to presses a key
    if (cv::waitKey(15) >= 0)
      break;
  }

  return 0;  
}

