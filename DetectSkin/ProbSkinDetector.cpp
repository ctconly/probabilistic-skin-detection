#include "stdafx.h"
#include "ProbSkinDetector.h"


ProbSkinDetector::ProbSkinDetector()
{
  ps = 0.25;
  pns = 1 - ps;
  threshold = 0.2;
  success = readHistograms("positives.bin", "negatives.bin");
}


ProbSkinDetector::ProbSkinDetector(char * posHistogramName, char * negHistogramName, double probs, double thresh)
{
  ps = probs;
  pns = 1 - probs;
  threshold = thresh;
  success = readHistograms(posHistogramName, negHistogramName);
}


ProbSkinDetector::~ProbSkinDetector()
{
  free(pos);
  free(neg);
}


cv::Mat ProbSkinDetector::detectSkin(cv::Mat img)
{
  /* Create the back projection image: here's the main algorithm:
  This calculates P(skin|color) for each image pixel. Then, one of two values is inserted in the resulting
  backprojection image. If the result of this calculation is above a certain value, that result is inserted
  into the image. Otherwise, a zero is inserted instead. This is primarily for viewing purposes.
  */
  backprojection.create(img.size(), CV_64FC1);

  int factor = 256 / posVertical; // what to divide by to get the bin number
  double pc_s;                    // P(color|skin)
  double pc_ns;                   // P(color|!skin)
  double pc;                      // P(color)
  double ps_c;                    // P(skin|color): what we are ultimately trying to calculate

  // Process each pixel in the image
  for (int row = 0; row < img.rows; row++)
  {
    for (int col = 0; col < img.cols; col++)
    {
      // Get the RGB values from the image
      cv::Vec3b pixelBGRValues = img.at<cv::Vec3b>(cv::Point(col, row));
      int red = pixelBGRValues[2];
      int green = pixelBGRValues[1];
      int blue = pixelBGRValues[0];

      // Calculate which bin we need from the histogram
      int binB = blue / factor;
      int binG = green / factor;
      int binR = red / factor;

      pc_s = pos[binB][binG][binR];     // Get P(color|skin) from the positives histogram

      factor = 256 / negVertical;
      pc_ns = neg[binB][binG][binR];    // Get P(color|!skin) from the negatives histogram
      pc = (pc_s * ps) + (pc_ns * pns); // Calculate P(color)

                                        // Avoid potential divide by zero issues
      if (pc == 0)
      {
        pc = 0.00000000001;
      }

      // Apply Bayes' rule to calculate P(skin|color)
      //  P(A|B) = P(B|A)P(A) / P(B)
      //  where P(B) = P(B|A)P(A) + P(B|!A)P(!A)
      ps_c = (pc_s * ps) / pc;

      // Insert P(skin|color) into backprojection for this pixel or 0 if it's below the threshold
      backprojection.at<double>(row, col) = (ps_c > threshold) ? ps_c : 0.0;
    }
  }

  return backprojection;
}

cv::Mat ProbSkinDetector::detectSkin(cv::Mat img, double probs, double probns, double thresh)
{
  ps = probs;
  probns = probns;
  threshold = thresh;
  return detectSkin(img);
}


bool ProbSkinDetector::readHistograms(char * posHistogramName, char * negHistogramName)
{
  // read positive histograms ----------------------------------------------
  FILE* fp = fopen(posHistogramName, "rb");
  if (fp == NULL)
  {
    printf("Unable to read positive histogram.\n");
    return false;
  }

  int *header;
  header = (int *)malloc(4 * sizeof(int));
  int sz = fread(header, sizeof(int), 4, fp);

  if (header[0] != 5 || sz != 4)
  {
    printf("Bad positives histogram file header: %d\n", sz);
    return false;
  }

  // dimensions of the histogram
  posVertical = header[1];
  posHorizontal = header[2];
  posDepth = header[3];

  // Create a 3D array to hold the positives histogram
  pos = (double***)malloc(posVertical * sizeof(double**));
  for (int i = 0; i < posVertical; i++)
  {
    pos[i] = (double**)malloc(posVertical * sizeof(double*));
    for (int j = 0; j < posHorizontal; j++)
    {
      pos[i][j] = (double*)malloc(posDepth * sizeof(double));
    }
  }

  // Read the values into the positives histogram
  double * buffer = (double*)malloc(posVertical * sizeof(double));
  for (int d = 0; d < posDepth; d++)
  {
    for (int c = 0; c < posHorizontal; c++)
    {
      fread(buffer, sizeof(double), posVertical, fp);
      for (int r = 0; r < posVertical; r++)
      {
        pos[r][c][d] = buffer[r];
      }
    }
  }
  free(buffer);
  fclose(fp);
  // end read positive histograms ----------------------------------------------

  // read negative histograms ----------------------------------------
  fp = fopen("negatives.bin", "rb");

  if (fp == NULL)
  {
    printf("Unable to read negative histogram.\n");
    return false;
  }

  sz = fread(header, sizeof(int), 4, fp);

  if (header[0] != 5 || sz != 4)
  {
    printf("Bad negatives histogram file header: %d\n", sz);
    return false;
  }

  // dimensions of the histogram
  negVertical = header[1];
  negHorizontal = header[2];
  negDepth = header[3];
  free(header);

  // Create a 3D array to hold the negatives histogram
  neg = (double***)malloc(negVertical * sizeof(double**));
  for (int i = 0; i < negVertical; i++)
  {
    neg[i] = (double**)malloc(negVertical * sizeof(double*));
    for (int j = 0; j < negHorizontal; j++)
    {
      neg[i][j] = (double*)malloc(negDepth * sizeof(double));
    }
  }

  // Read the values into the negatives histogram
  buffer = (double*)malloc(negVertical * sizeof(double));
  for (int d = 0; d < negDepth; d++)
  {
    for (int c = 0; c < negHorizontal; c++)
    {
      fread(buffer, sizeof(double), negVertical, fp);
      for (int r = 0; r < negVertical; r++)
      {
        neg[r][c][d] = buffer[r];
      }
    }
  }
  free(buffer);
  fclose(fp);
  // end read negative histograms ------------------------------------

  return true;
}
