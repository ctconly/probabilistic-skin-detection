#pragma once
class ProbSkinDetector
{
public:
  ProbSkinDetector();
  ProbSkinDetector(char * posHistogramName, char * negHistogramName, double probs, double thresh);
  ~ProbSkinDetector();
  
  cv::Mat detectSkin(cv::Mat img);
  cv::Mat detectSkin(cv::Mat img, double probs, double probns, double thresh);

  bool success;

private:
  int posVertical;        // # of bins in the vertical dimension of the positives histogram
  int posHorizontal;      // # of bins in the horizontal dimension of the positives histogram
  int posDepth;           // # of bins in the depth dimension of the positives histogram
  int negVertical;        // # of bins in the vertical dimension of the negatives histogram
  int negHorizontal;      // # of bins in the horizontal dimension of the negatives histogram
  int negDepth;           // # of bins in the depth dimension of the positives histogram
  double ps;              // P(S): the initial probability that a pixel is skin
  double pns;             // P(!S): the initial probability that a pixel is not skin
  double threshold;       // A threshold to keep pixels as potential skin
  double ***pos;          // positives histogram
  double ***neg;          // negatives histogram
  cv::Mat backprojection; // skin probability image

  bool readHistograms(char * posHistogramName, char * negHistogramName);
};

